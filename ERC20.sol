// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./IERC20.sol";

contract ERC20 is IERC20 {
    address _owner;
    address _minter;
    string _name;
    string _symbol;
    uint _totalTokens;
    mapping(address => uint) balances;
    mapping(address => mapping(address => uint)) allowances;

    constructor(string memory name_, string memory symbol_, uint initialSupplly) {
        _name = name_;
        _symbol = symbol_;
        _owner = msg.sender;
        mint(initialSupplly);
    }

    modifier enoughtTokens(address _from, uint _amount) {
        require(balanceOf(_from) >= _amount, "not enought tokens!");
        _;
    }

    modifier onlyOwner() {
        require(msg.sender == _owner, "not an owner!");
        _;
    }

    modifier onlyOwnerAndMinter() {
        require(msg.sender == _minter || msg.sender == _owner);
        _;
    }

    function mint(uint amount) public onlyOwnerAndMinter {
        balances[_owner] += amount;
        _totalTokens += amount;

        emit Transfer(address(0), _owner, amount);
    }

    function burn(address from, uint amount) public onlyOwnerAndMinter {
        _beforeTokenTransfer(from, address(0), amount);

        balances[from] -=amount;
        _totalTokens -=amount;
    }

    function assignMinter(address minter) external onlyOwner {
        _minter = minter;
    }

    function name() external override view returns(string memory) {
        return _name;
    }

    function symbol() external override view returns(string memory) {
        return _symbol;
    }

    function decimals() external override pure returns(uint8) {
        return 18;
    }

    function totalSupply() external override view returns(uint) {
        return _totalTokens;
    }

    function balanceOf(address account) public override view returns(uint) {
        return balances[account];
    }

    function transfer(address to, uint amount) external override enoughtTokens(msg.sender, amount) {
        _beforeTokenTransfer(msg.sender, to, amount);
        balances[msg.sender] -= amount;
        balances[to] += amount;

        emit Transfer(msg.sender, to, amount);
    }

    function allowance(address owner, address spender) public override view returns(uint) {
        return allowances[owner][spender]; 
    } 

    function approve(address spender, uint amount) external override {
        _approve(msg.sender, spender, amount);
    }

    function transferFrom(address sender, address recipient, uint amount) external override enoughtTokens(sender, amount) {
        _beforeTokenTransfer(sender, recipient, amount);

        require(allowances[sender][recipient] >= amount, "check allowance!");
        allowances[sender][recipient] -= amount;
        
        balances[sender] -= amount;
        balances[recipient] += amount;

        emit Transfer(sender, recipient, amount);
    }
    
    function _approve(address owner, address spender, uint amount) internal virtual
    {
        allowances[owner][spender] = amount;

        emit Approve(owner, spender, amount);
    }

    function _beforeTokenTransfer(address from, address to, uint amount) internal virtual {
        require(from != address(0), "You address is null");
        require(to != address(0), "You address is null");
        require(balances[from] >= amount, "You don't have the resources");
    }
}