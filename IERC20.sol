// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

interface IERC20 {
    function name() external view returns(string memory); //название токена (не является стандартом, но рекомендуется)

    function symbol() external view returns(string memory); //Обозначение токена

    function decimals() external pure returns(uint8); // Указывается сколько будет знаков после запятой, например эфир = 1 * 10^18

    function totalSupply() external view returns(uint); 

    function balanceOf(address account) external view returns(uint);

    function transfer(address to, uint amount) external;

    function allowance(address owner, address spender) external view returns(uint);

    function approve(address spender, uint amount) external;

    function transferFrom(address sender, address recipient, uint amount) external;

    event Transfer(address indexed from, address indexed to, uint amount);
    
    event Approve(address indexed owner, address indexed to, uint amount);
}